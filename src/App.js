import React, {Component} from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as Actions from './app/home/duck/actions'
import reducer from './app/home/duck/reducer'

export class App extends Component {
  render() {
    return (
      <div>
        <div>Your age {this.props.age}</div>
        <button onClick={this.props.onAgeUp}>Age Up</button>
        <button onClick={this.props.onAgeDown}>Age Down</button>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    age: state.reducer.age
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(Actions,dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
  )(App)