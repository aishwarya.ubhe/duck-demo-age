import types from './types'

const ageUp = (value) => ({
  type: types.AGE_UP,
  value:1
});

const ageDown = (value) => ({
  type: types.AGE_DOWN,
  value: 1
});

export default {
  ageUp,
  ageDown
}

// import { createActions } from 'reduxsauce';

// const { Creators, Types } = createActions({
//   increment: ['value'],
//   decrement: ['value']
// });

// export { Creators, Types };




