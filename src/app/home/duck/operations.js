import Creators from './actions'
const incrementAge = Creators.incrementAge;
const decreamentAge = Creators.decreamentAge;

export default {
  incrementAge,
  decreamentAge
}