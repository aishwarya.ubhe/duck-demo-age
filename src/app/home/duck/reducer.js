import types from './types'
const initialState = {
  age: 1
}

const reducer = (state=initialState,action) => {
  const newState = {...state};
  switch(action.type) {
    case types.incrementAge: {
      newState.age += action.value
      break;
    }
    case types.decreamentAge: {
      newState.age -= action.value
      break;
    }
    return newState
  }
}

export default reducer

